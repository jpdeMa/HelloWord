//
//  ContentView.swift
//  HelloWorkd
//
//  Created by Jean-Pierre on 15/03/2021.
//  Copyright © 2021 Jean-Pierre. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        HStack {
            Image(systemName: "sun.max")
            VStack {
                Text("Welcome, Git World!")
                Text("Osaka, 2021")
            }
        }
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
